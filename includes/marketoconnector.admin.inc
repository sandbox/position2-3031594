<?php
/**
* @file
* Administrator configuration form for marketo connector
*/

function marketoconnector_admin_settings_form($form, &$form_state) {
	$form['marketoconnector_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
	$form['marketoconnector_tabs']['marketoconnector_basic'] = array(
    '#title' => t('General Settings'),
    '#type' => 'fieldset',
    '#description' => t('Marketo account access credential.'),
  );
	$form['marketoconnector_tabs']['marketoconnector_basic']['marketoconnector_munchkin_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketo Munchkin Account ID'),
    '#default_value' => variable_get('marketoconnector_munchkin_account_id', ''),
    '#required' => TRUE,
  );
  $form['marketoconnector_tabs']['marketoconnector_basic']['marketoconnector_marketo_soap_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketo Soap EndPoint'),
    '#default_value' => variable_get('marketoconnector_marketo_soap_endpoint', ''),
    '#required' => TRUE,
  );
  $form['marketoconnector_tabs']['marketoconnector_basic']['marketoconnector_marketo_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketo User Id'),
    '#default_value' => variable_get('marketoconnector_marketo_user_id', ''),
    '#required' => TRUE,
  );
  $form['marketoconnector_tabs']['marketoconnector_basic']['marketoconnector_marketo_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketo Secret Key'),
    '#default_value' => variable_get('marketoconnector_marketo_secret_key', ''),
    '#required' => TRUE,
  );

  $form['marketoconnector_tabs']['marketoconnector_api'] = array(
    '#title' => t('Auto-Email Blog Updates'),
    '#type' => 'fieldset',
    '#description' => t('Set up to send new post updates to subscribers here:'),
  );
  $form['marketoconnector_tabs']['marketoconnector_api']['marketoconnector_marketo_program'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketo Program'),
    '#default_value' => variable_get('marketoconnector_marketo_program', ''),
    '#required' => TRUE,
  );
  $form['marketoconnector_tabs']['marketoconnector_api']['marketoconnector_marketo_campaign'] = array(
    '#type' => 'textfield',
    '#title' => t('Marketo Campaign'),
    '#default_value' => variable_get('marketoconnector_marketo_campaign', ''),
    '#required' => TRUE,
  );
  $contenttype_options = array_merge(array("" => t('select type')), node_type_get_names());
  $form['marketoconnector_tabs']['marketoconnector_api']['marketoconnector_allowed_contenttype'] = array(
    '#type' => 'select',
    '#title' => t('Allowed Content Type:'),
    '#default_value' => variable_get('marketoconnector_allowed_contenttype', array()),
    '#options' => $contenttype_options,
    '#required' => TRUE,
  );	
  $form = _taxonomy_terms_by_vocabulary($form);
  $form = system_settings_form($form);
  $form['#submit'][] = '_marketoconnector_system_settings_form_submit';
  return $form;
}
function _marketoconnector_system_settings_form_submit($form, &$form_state) {

  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  if (!empty($form_state['values']['marketoconnector_allowed_contenttype'])) {
	  _marketoconnector_pushed_flag_field($form_state['values']['marketoconnector_allowed_contenttype']);  	
  }
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }
}
function _marketoconnector_pushed_flag_field($content_type) {
	$myField_name = "field_marketo_pushed";
	if(!field_info_field($myField_name)) { // check if the field already exists.
    $field = array(
      'field_name'    => $myField_name,
      'type'          => 'number_integer',
    );
    field_create_field($field);
    $field_instance = array(
      'field_name'    => $myField_name,
      'entity_type'   => 'node',
      'bundle'        => $content_type,
      'default_value' => array(array('value' => 0)),
      'label' => t('Integer'),
      'description' => t('This field stores flag(0 or 1), to confirm content pushed to marketo or not.'),
      'instance_settings' => array(
        'min' => '',
        'max' => '',
        'prefix' => '',
        'suffix' => '',
      ),
      'default_widget' => 'number',
      'default_formatter' => 'number_integer',
      'display' => array(
        'default' => array('type' => 'hidden'),
      ),
    );
    field_create_instance($field_instance);
	}
}
function _taxonomy_terms_by_vocabulary($form) {
	$taxonomy_term_reference = field_info_field_map();
	$contentTypeTerms = array();
	foreach ($taxonomy_term_reference as $field => $value) {
		if ($value['type'] == 'taxonomy_term_reference') {
  		$options = array();
			foreach (field_info_field($field)['settings']['allowed_values'] as $tree) {
		    if ($vocabulary = taxonomy_vocabulary_machine_name_load($tree['vocabulary'])) {
		      if ($terms = taxonomy_get_tree($vocabulary->vid, $tree['parent'])) {
		        foreach ($terms as $term) {
		          $options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
		        }
		      }
		    }
		  }
			$contentTypeTerms['bundles'][] = array(
																			   "node_types" => $value['bundles']['node'],
																			   "term_reference_field" => $field,
																			   "vocabulary_machine_name" => $vocabulary->machine_name,
																			   "vocabulary_name" => $vocabulary->name,
																			   "terms" => $options
																			 );
		}
	}
	$bundleClass = array();
	foreach ($contentTypeTerms['bundles'] as $bundleVal) {
		if (isset($bundleClass[$bundleVal['vocabulary_machine_name']])) {
			$bundleClass[$bundleVal['vocabulary_machine_name']] .= " " . implode(" ", $bundleVal['node_types']);
		} else {
			$bundleClass[$bundleVal['vocabulary_machine_name']] = implode(" ", $bundleVal['node_types']);
		}
		if (!empty($bundleVal['terms'])) {
			$form['marketoconnector_tabs']['marketoconnector_api']['marketoconnector_allowed_' . $bundleVal['vocabulary_machine_name']] = array(
		    '#type' => 'checkboxes',
		    '#title' => $bundleVal['vocabulary_name'],
		    '#default_value' => variable_get('marketoconnector_allowed_' . $bundleVal['vocabulary_machine_name'], array()),
		    '#options' => $bundleVal['terms'],
		    '#attributes' => array('class' => array($bundleClass[$bundleVal['vocabulary_machine_name']]))
		  );
		}
	}
  return $form;
}