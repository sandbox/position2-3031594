(function($){
	var changeManual = false;
	jQuery( document ).delegate( "#edit-marketoconnector-allowed-contenttype", "change", function() {
		var selecedOptionVal = jQuery( this ).val();
		jQuery( "#marketoconnector-admin-settings-form div.form-type-checkboxes" ).css( "display", "none" );
		if (jQuery( "div." + selecedOptionVal ).length > 0) {
			jQuery( "div." + selecedOptionVal ).parent().css( "display", "block" )
			if(!changeManual) {
				jQuery( "div." + selecedOptionVal ).parent().find("input[type='checkbox']").attr("checked",false);
			}
		}
		if(changeManual) {
			changeManual = false;
		}
	});
	jQuery(window).load(function() {
		changeManual = true;
		jQuery("#edit-marketoconnector-allowed-contenttype").trigger("change");
	});
})(jQuery);
